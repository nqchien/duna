# Duna

## Overview
This repository contains Matlab code of Duna model for wind-driven morphology change of coastal profiles.

The model is detailed in the paper Roelvink and Costas, _Environ Model Softw_ (2019).

## Applications
The model has originally been applied for the case of Praia de Faro, Portugal (Roelvink and Costas 2019).

The model is newly applied for Ynyslas beach, Wales. The tide data is retrieved from Robins et al., _J Coast Conserv_ (2011). The wave data is retrieved from a monitoring buoy in the Irish Sea, combined with reanalysed wave data. The beach profile data is extracted from LiDAR images.


## References
* Roelvink D, Costas S (2019) Coupling nearshore and aeolian processes: XBeach and duna process-based models. _Environmental Modelling & Software_ 115:98-112.
* Robins PE, Davies AG, Jones R (2011) Application of a coastal model to simulate present and future inundation and aid coastal management. _Journal of Coastal Conservation_ 15(1):1-14.
